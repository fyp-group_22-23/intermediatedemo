# Screenshots of the demo application

## About
![](about.png)

## Control
![](control.png)

## Map 1:1 simulation
![](map.png)

## WebSocket playground
![](topics.png)

## Confiugration page
![](config.png)
