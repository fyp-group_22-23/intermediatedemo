# Intermediate Demo Application

This is the source code for our demo application, the snapshot of the latest stable build.

For the developing build go visit https://gitlab.com/timescam/intermediateDemo


[Download here](https://gitlab.com/fyp-group_22-23/intermediatedemo/-/raw/main/intermediateDemo-main.zip?inline=false)
